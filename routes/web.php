<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use app\Http\Controllers\PresignupController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Route::get('/presignup', 'PresignupController@index')->name('presignup.index');
Route::get('/presignup/create','PresignupController@create')->name('presignup.create');
*/
Route::post('/presignup', 'PresignupController@store')->name('presignup.store');

