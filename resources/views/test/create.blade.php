<form class="ui huge form" action="{{route('presignup.store')}}" method="post" id="sign-up">
    @csrf
    <div class="two fields">
        <div class=" required field">
            <label>First Name</label>
            <input type="text" name="first_name" placeholder="First Name">
        </div>
        <div class=" required field">
            <label>Second Name</label>
            <input type="text" name="last_name" placeholder="Last Name">
        </div>
    </div>

    <div class="two fields">
        <div class=" required field">
            <label>Email</label>
            <input type="text" name="email" placeholder="Email">
        </div>
        <div class="field">
            <label>Mobile</label>
            <input type="text" name="mobile" placeholder="Mobile">
        </div>
    </div>

    <div class="inline centered fields">
        <div class="field" style="margin: auto;padding-top:1rem;padding-bottom:1rem;">
            <div class="ui checkbox">
            <input type="checkbox" name="roles[]" value="speaker" tabindex="0" class="hidden">
            <label>I’m a speaker</label>
            </div>
        </div>
        <div class="field" style="margin: auto;padding-top:1rem;padding-bottom:1rem;">
            <div class="ui checkbox">
            <input type="checkbox" name="roles[]" value="booker" tabindex="0" class="hidden">
            <label>I’m a booker for media and / or events</label>
            </div>
        </div>
    </div>

    <!--
    <div class="ui segment">
        <div class="field">
            <div class="ui checkbox" id="lanyard">
                <input type="checkbox" name="gift" tabindex="0" class="hidden">
                <label style="font-size:1.4rem;">Please send me a <a target="_blank" href="https://npwlive.com">NPWLive</a> lanyard.</label>
            </div>
        </div>
    </div>
-->
    

    <div class="address">

    <br /><br />
    <p>Please send me a limited edition NPWLive lanyard.</p>
        <br /><br />

    <div class="two fields">
        <div class="required field">
            <label>Address Line 1</label>
            <input name="line1" value="" type="text" placeholder="Address Line 1">
        </div>
        <div class="required field">
            <label>Address Line 2</label>
            <input name="line2" value="" type="text" placeholder="Address Line 2">
        </div>
    </div>


    <div class="two fields">
        <div class="field">
            <label>Address Line 3</label>
            <input name="line3" value="" type="text" placeholder="Address Line 3">
        </div>
        <div class="field">
            <label>Address Line 4</label>
            <input name="line4" value="" type="text" placeholder="Address Line 4">
        </div>
    </div>

    <div class="two fields">
        <div class="required field">
            <label>County</label>
            <div class="ui fluid normal dropdown selection" tabindex="0"><select name="county" class="noselection"><option selected="selected" value="">County</option><option value="Avon">Avon</option><option value="Bedfordshire">Bedfordshire</option><option value="Berkshire">Berkshire</option><option value="Buckinghamshire">Buckinghamshire</option><option value="Cambridgeshire">Cambridgeshire</option><option value="Cheshire">Cheshire</option><option value="Cleveland">Cleveland</option><option value="Cornwall">Cornwall</option><option value="Cumbria">Cumbria</option><option value="Derbyshire">Derbyshire</option><option value="Denbighshire">Denbighshire</option><option value="Devon">Devon</option><option value="Dorset">Dorset</option><option value="Durham">Durham</option><option value="East Sussex">East Sussex</option><option value="Essex">Essex</option><option value="Gloucestershire">Gloucestershire</option><option value="Hampshire">Hampshire</option><option value="Herefordshire">Herefordshire</option><option value="Hertfordshire">Hertfordshire</option><option value="Isle of Wight">Isle of Wight</option><option value="Kent">Kent</option><option value="Lancashire">Lancashire</option><option value="Leicestershire">Leicestershire</option><option value="Lincolnshire">Lincolnshire</option><option value="London">London</option><option value="Merseyside">Merseyside</option><option value="Middlesex">Middlesex</option><option value="Norfolk">Norfolk</option><option value="Northamptonshire">Northamptonshire</option><option value="Northumberland">Northumberland</option><option value="North Humberside">North Humberside</option><option value="North Yorkshire">North Yorkshire</option><option value="Nottinghamshire">Nottinghamshire</option><option value="Oxfordshire">Oxfordshire</option><option value="Rutland">Rutland</option><option value="Shropshire">Shropshire</option><option value="Somerset">Somerset</option><option value="South Humberside">South Humberside</option><option value="South Yorkshire">South Yorkshire</option><option value="Staffordshire">Staffordshire</option><option value="Suffolk">Suffolk</option><option value="Surrey">Surrey</option><option value="Tyne and Wear">Tyne and Wear</option><option value="Warwickshire">Warwickshire</option><option value="West Midlands">West Midlands</option><option value="West Sussex">West Sussex</option><option value="West Yorkshire">West Yorkshire</option><option value="Wiltshire">Wiltshire</option><option value="Worcestershire">Worcestershire</option></select><i class="dropdown icon"></i><div class="default text">County</div><div class="menu transition hidden" tabindex="-1"><div class="item" data-value="Avon" data-text="Avon">Avon</div><div class="item" data-value="Bedfordshire" data-text="Bedfordshire">Bedfordshire</div><div class="item" data-value="Berkshire" data-text="Berkshire">Berkshire</div><div class="item" data-value="Buckinghamshire" data-text="Buckinghamshire">Buckinghamshire</div><div class="item" data-value="Cambridgeshire" data-text="Cambridgeshire">Cambridgeshire</div><div class="item" data-value="Cheshire" data-text="Cheshire">Cheshire</div><div class="item" data-value="Cleveland" data-text="Cleveland">Cleveland</div><div class="item" data-value="Cornwall" data-text="Cornwall">Cornwall</div><div class="item" data-value="Cumbria" data-text="Cumbria">Cumbria</div><div class="item" data-value="Derbyshire" data-text="Derbyshire">Derbyshire</div><div class="item" data-value="Denbighshire" data-text="Denbighshire">Denbighshire</div><div class="item" data-value="Devon" data-text="Devon">Devon</div><div class="item" data-value="Dorset" data-text="Dorset">Dorset</div><div class="item" data-value="Durham" data-text="Durham">Durham</div><div class="item" data-value="East Sussex" data-text="East Sussex">East Sussex</div><div class="item" data-value="Essex" data-text="Essex">Essex</div><div class="item" data-value="Gloucestershire" data-text="Gloucestershire">Gloucestershire</div><div class="item" data-value="Hampshire" data-text="Hampshire">Hampshire</div><div class="item" data-value="Herefordshire" data-text="Herefordshire">Herefordshire</div><div class="item" data-value="Hertfordshire" data-text="Hertfordshire">Hertfordshire</div><div class="item" data-value="Isle of Wight" data-text="Isle of Wight">Isle of Wight</div><div class="item" data-value="Kent" data-text="Kent">Kent</div><div class="item" data-value="Lancashire" data-text="Lancashire">Lancashire</div><div class="item" data-value="Leicestershire" data-text="Leicestershire">Leicestershire</div><div class="item" data-value="Lincolnshire" data-text="Lincolnshire">Lincolnshire</div><div class="item" data-value="London" data-text="London">London</div><div class="item" data-value="Merseyside" data-text="Merseyside">Merseyside</div><div class="item" data-value="Middlesex" data-text="Middlesex">Middlesex</div><div class="item" data-value="Norfolk" data-text="Norfolk">Norfolk</div><div class="item" data-value="Northamptonshire" data-text="Northamptonshire">Northamptonshire</div><div class="item" data-value="Northumberland" data-text="Northumberland">Northumberland</div><div class="item" data-value="North Humberside" data-text="North Humberside">North Humberside</div><div class="item" data-value="North Yorkshire" data-text="North Yorkshire">North Yorkshire</div><div class="item" data-value="Nottinghamshire" data-text="Nottinghamshire">Nottinghamshire</div><div class="item" data-value="Oxfordshire" data-text="Oxfordshire">Oxfordshire</div><div class="item" data-value="Rutland" data-text="Rutland">Rutland</div><div class="item" data-value="Shropshire" data-text="Shropshire">Shropshire</div><div class="item" data-value="Somerset" data-text="Somerset">Somerset</div><div class="item" data-value="South Humberside" data-text="South Humberside">South Humberside</div><div class="item" data-value="South Yorkshire" data-text="South Yorkshire">South Yorkshire</div><div class="item" data-value="Staffordshire" data-text="Staffordshire">Staffordshire</div><div class="item" data-value="Suffolk" data-text="Suffolk">Suffolk</div><div class="item" data-value="Surrey" data-text="Surrey">Surrey</div><div class="item" data-value="Tyne and Wear" data-text="Tyne and Wear">Tyne and Wear</div><div class="item" data-value="Warwickshire" data-text="Warwickshire">Warwickshire</div><div class="item" data-value="West Midlands" data-text="West Midlands">West Midlands</div><div class="item" data-value="West Sussex" data-text="West Sussex">West Sussex</div><div class="item" data-value="West Yorkshire" data-text="West Yorkshire">West Yorkshire</div><div class="item" data-value="Wiltshire" data-text="Wiltshire">Wiltshire</div><div class="item" data-value="Worcestershire" data-text="Worcestershire">Worcestershire</div></div></div></div>
        <div class="required field">
            <label>Postcode</label>
            <input name="postcode" value="" type="text" placeholder="Postcode">
        </div>
    </div>

    </div>

    <div class="ui error message"></div>

    <button style="background-color:#6DC6D9;color:white;" class="ui huge right floated button" type="submit">I want to be heard!</button>
</form>