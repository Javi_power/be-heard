<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::create([
            'email'=>'test@test.com',
            'password'=>Hash::make('12345678')
        ]);

        $user->save();
        $user->roles()->save( (Role::where('role','super')->first() ) );
        

        $user1 = User::create([
            'email'=>'test1@test1.com',
            'password'=>Hash::make('12345678')
        ]);

        $user1->save();
        $user1->roles()->save( (Role::where('role','user')->first() ) );

        $user3 = User::create([
            'email'=>'user3@user3.com',
            'password'=>Hash::make('888888888')
        ]);

        $user3->save();
        $user3->roles()->save( (Role::where('role','agent')->first() ) );
    }
}
