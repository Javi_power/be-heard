<?php

namespace Database\Seeders;

use App\Models\Assest;
use Illuminate\Database\Seeder;

class AssestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $assest = Assest::create([
            'pro_id'=>'1',
            'name'=>'video',
            'description'=>'video of the new platform',
            'filename'=>'storage/videos/video2.mp4',
            'filesize_kilobytes'=> 123333333,
            'moderated'=> true,
            'type'=>'musical',
            'or_filename'=>'storage/video'
        ]);

        $assest->save();

        $assest1 = Assest::create([
            'pro_id'=>'2',
            'name'=>'music',
            'description'=>'music for the party of the new platform',
            'filename'=>'storage/videos/audio.mp3',
            'filesize_kilobytes'=> 123333,
            'moderated'=> false,
            'type'=>'musical',
            'or_filename'=>'storage/video'
        ]);
        
        $assest1->save();
    }

}
