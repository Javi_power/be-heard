<?php

namespace Database\Seeders;

use App\Models\Expertise;
use App\Models\Profile;
use App\Models\Speaker;
use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profile = Profile::create([
            'usr_id'=>1,
            'adr_id'=>1,
            'sec_id'=>2,
            'name'=>'javier',
            'surname'=>'candela',
            'phone'=> '+44589632575',
            'contactable'=>false,
            'bio'=>'I was living in New York....',
            'url_photo'=> 'storage/myphoto.png',
            'disability'=>'none',
            'Ethnicity'=> 'white',
            'pronouns'=>'Mr',

        ]);

        $profile->save();
        $profile->expertises()->save(Expertise::where('aex_id',2)->first());
        $profile->speakers()->save(Speaker::where('spk_id',5)->first());

        $profile1 = Profile::create([
            'usr_id'=>2,
            'adr_id'=>2,
            'sec_id'=>5,
            'name'=>'peter',
            'surname'=>'parker',
            'phone'=> '+44589632575',
            'contactable'=>false,
            'bio'=>'I was playing soccer....',
            'url_photo'=> 'storage/myphoto.png',
            'disability'=>'none',
            'Ethnicity'=> 'white',
            'pronouns'=>'Mr',

        ]);

        $profile1->save();
        $profile1->expertises()->save(Expertise::where('aex_id',4)->first());
        $profile1->speakers()->save(Speaker::where('spk_id',4)->first());


        
    }





}
