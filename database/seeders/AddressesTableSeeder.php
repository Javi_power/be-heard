<?php

namespace Database\Seeders;

use App\Models\Address;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $address = Address::create([
            'line1'=> 'Mold Road',
            'line2'=> 'test',
            'line3'=> '',
            'line4'=> '',
            'county' => 'Denbighshire',
            'postcode' => 'LL15 1SL'
        ]);

        $address->save();

        $address1 = Address::create([
            'line1'=> 'Duke Street',
            'line2'=> '100',
            'line3'=> '',
            'line4'=> '',
            'county' => 'Liverpool',
            'postcode' => 'LL15 1SL'
        ]);

        $address1->save();


        
        
       
    }
}
