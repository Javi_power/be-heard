<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class AreasExpertiseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        DB::table('areas_expertise')->insert(array(
            array('description'=>'Advertising and marketing'),
            array('description'=>'Big data'),
            array('description'=>'Business development sales'),
            array('description'=>'Business transformation'),
            array('description'=>'Change management'),
            array('description'=>'Coaching'),
            array('description'=>'Communication skills'),
            array('description'=>'Communications'),
            array('description'=>'Community/civic society'),
            array('description'=>'Computer and technical skills'),
            array('description'=>'Conflict resolution'),
            array('description'=>'Contract negotiation'),
            array('description'=>'Creative thinking'),
            array('description'=>'Critical thinking'),
            array('description'=>'Culture'),
            array('description'=>'Data and metrics analysis'),
            array('description'=>'Design'),
            array('description'=>'Digital & coding'),
            array('description'=>'Education'),
            array('description'=>'Emergency planning'),
            array('description'=>'Engineering'),
            array('description'=>'Entrepreneurship'),
            array('description'=>'Events'),
            array('description'=>'Financial planning'),
            array('description'=>'Funding/bid writing'),
            array('description'=>'General management & leadership'),
            array('description'=>'Health & social care'),
            array('description'=>'Human resources'),
            array('description'=>'Interview skills'),
            array('description'=>'Marketing and Branding'),
            array('description'=>'Media'),
            array('description'=>'Mental health'),
            array('description'=>'Mentoring'),
            array('description'=>'Networking'),
            array('description'=>'Neurodiversity'),
            array('description'=>'People management'),
            array('description'=>'Personal profile'),
            array('description'=>'Podcasts'),
            array('description'=>'Policy & public life'),
            array('description'=>'Presentation skills'),
            array('description'=>'Project management'),
            array('description'=>'Psychology'),
            array('description'=>'Public-speaking'),
            array('description'=>'Recovery & resilience'),
            array('description'=>'Research & development'),
            array('description'=>'Risk management'),
            array('description'=>'STEM & STEAM'),
            array('description'=>'Strategic planning'),
            array('description'=>'Time management'),
            array('description'=>'Training'),
            array('description'=>'Volunteering'),
            array('description'=>'Web development'),
            array('description'=>'Webinars'),
            array('description'=>'Wellbeing & fitness'),

        ));
    }
}
