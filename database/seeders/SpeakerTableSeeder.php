<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class SpeakerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('speakers')->insert(array(
            array('description'=>'Live Events'),
            array('description'=>'TV'),
            array('description'=>'Radio'),
            array('description'=>'Virtual events'),
            array('description'=>'Keynote'),
            array('description'=>'Panels'),
            array('description'=>'Webinars'),
            array('description'=>'Podcast'),
            array('description'=>'Stage Interviews'),
            array('description'=>'Tedx talks'),
            array('description'=>'Conference'),
            array('description'=>'Event host'),
            array('description'=>'After dinner speakier')

        ));


    }
}
