<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;



class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('roles')->insert([
            'role'   		=> 'super',
			'description'	=> 'Super user'
        ]);
        DB::table('roles')->insert([
            'role'   		=> 'user',
			'description'	=> 'Profile user'
        ]);
        DB::table('roles')->insert([
            'role'   		=> 'agent',
			'description'	=> 'agent user'
        ]);
    }
}
