<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class SectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sectors')->insert(array(
            array('description'=>'Accountancy/banking/finance'),
            array('description'=>'Business consultancy and managment'),
            array('description'=>'Charity and voluntary work'),
            array('description'=>'Creative arts and design'),
            array('description'=>'Energy and utilities'),
            array('description'=>'Engineering and manufacturing'),
            array('description'=>'Environment and agriculture'),
            array('description'=>'Healthcare'),
            array('description'=>'Hospitality and events management'),
            array('description'=>'Information technology'),
            array('description'=>'Law'),
            array('description'=>'Law enforcement and security'),
            array('description'=>'Lesisure sports and tourism'),
            array('description'=>'Marketing, advertising & pr'),
            array('description'=>'Media and internet'),
            array('description'=>'Public services and administration'),
            array('description'=>'Recruitment and hr'),
            array('description'=>'Retail'),
            array('description'=>'Regeneration land and property'),
            array('description'=>'Sales'),
            array('description'=>'Science and pharmacutical'),
            array('description'=>'Social care'),
            array('description'=>'Teaching, training and education'),
            array('description'=>'Transport and logistics'),
            array('description'=>'In education or training'),
            array('description'=>'Not working'),
            array('description'=>'Not in education employment or training'),
            array('description'=>'Multi sector'),

        ));
    }
}
