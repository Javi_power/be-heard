<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreSignupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presignups', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('mobile')->nullable(); //String because maybe someone need to add +44
            $table->string('line1')->nullable();
			$table->string('line2')->nullable();
			$table->string('line3')->nullable();
			$table->string('line4')->nullable();
			$table->string('county')->nullable();
			$table->string('postcode')->nullable();
            $table->boolean('speaker')->default(false);
            $table->boolean('booker')->default(false);
            $table->timestamps();
        });

      

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_signup');
    }
}
