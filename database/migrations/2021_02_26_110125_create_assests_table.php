<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assests', function (Blueprint $table) {
            $table->id('ass_id');
            
            $table->unsignedBigInteger('pro_id');
            
            $table->string('name');
            $table->string('description');
            $table->string('filename');
            $table->integer('filesize_kilobytes');
            $table->boolean('moderated')->default(false);
            $table->string('type');
            $table->string('or_filename');
        
            $table->timestamps();

            $table->foreign('pro_id')->references('pro_id')->on('profiles');
        });

        
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assests');
    }
}
