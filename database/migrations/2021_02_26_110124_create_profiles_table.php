<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sectors', function(Blueprint $table){
            $table->id('sec_id');
            $table->string('description');
        });

        Schema::create('speakers', function(Blueprint $table){
            $table->id('spk_id');
            $table->string('description');
        });

        Schema::create('areas_expertise', function(Blueprint $table){
            $table->id('aex_id');
            $table->string('description');
        });

        
        Schema::create('profiles', function (Blueprint $table) {
            $table->id('pro_id');
            
            $table->unsignedBigInteger('usr_id');
            $table->unsignedBigInteger('adr_id');
            $table->unsignedBigInteger('sec_id');
    
            $table->string('name');
            $table->string('surname');
            $table->string('phone')->nullable(); //String because maybe someone need to add +44
            $table->boolean('contactable')->default(false);
            $table->text('bio');
            $table->string('url_photo')->nullable();
            $table->string('disability')->nullable();
            $table->string('ethnicity')->nullable();
            $table->string('pronouns')->nullable();
            $table->string('facebook')->nullable();
			$table->string('twitter')->nullable();
			$table->string('instagram')->nullable();
            $table->string('url_videos')->nullable();
            $table->string('url_audios')->nullable();

            $table->timestamps();

            
            $table->foreign('usr_id')->references('usr_id')->on('users');
            $table->foreign('adr_id')->references('adr_id')->on('addresses');
            $table->foreign('sec_id')->references('sec_id')->on('sectors');
        });

        
        

        
        Schema::create('profile_speaker', function(Blueprint $table){
            $table->id('pro_spk_id');
            $table->unsignedBigInteger('pro_id');
            $table->unsignedBigInteger('spk_id');

            $table->foreign('pro_id')->references('pro_id')->on('profiles');
            $table->foreign('spk_id')->references('spk_id')->on('speakers');
            
        });


        
        Schema::create('expertise_profile', function(Blueprint $table){
            $table->id('exp_pro_id');
            $table->unsignedBigInteger('pro_id');
            $table->unsignedBigInteger('aex_id');

            $table->foreign('pro_id')->references('pro_id')->on('profiles');
            $table->foreign('aex_id')->references('aex_id')->on('areas_expertise');
        });

       

        


        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
