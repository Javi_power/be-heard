<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
    */
    


    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('usr_id');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('roles', function(Blueprint $table){
            $table->id('rol_id');
            $table->string('role');
            $table->string('description');
        });

        Schema::create('user_role', function(Blueprint $table){
            $table->id('usr_rol_id');
            $table->unsignedBigInteger('usr_id');
            $table->unsignedBigInteger('rol_id');
            $table->foreign('usr_id')->references('usr_id')->on('users');
            $table->foreign('rol_id')->references('rol_id')->on('roles');
            
        });

        Schema::create('user_activations', function (Blueprint $table) {
            $table->id('activations_id');
			$table->unsignedBigInteger('usr_id');
            $table->string('token');
            
			$table->foreign('usr_id')->references('usr_id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('user_role');
        Schema::dropIfExists('user_activation');
    }
}
