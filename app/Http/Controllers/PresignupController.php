<?php

namespace App\Http\Controllers;

use App\Models\Presignup;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PresignupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'mobile'=> 'nullable',
            'roles' => 'required',
            'line1'=> 'nullable',
            'line2'=> 'nullable',
            'line3'=> 'nullable',
            'line4'=> 'nullable',
            'county'=> 'nullable',
            'postcode'=> 'nullable',
        ]);

        $roles = $data['roles'];
        $speaker = 0;
        $booker= 0;
        if(in_array("speaker", $roles)){
            $speaker = 1;
        }
        if(in_array("booker", $roles)){
            $booker = 1;
        }
        
        
        DB::table('presignups')->insert([
            'first_name' =>$data['first_name'],
            'last_name' =>$data['last_name'],
            'email' =>$data['email'],
            'mobile' =>$data['mobile'],
            'line1' =>$data['line1'],
            'line2' =>$data['line2'],
            'line3' =>$data['line3'],
            'line4' =>$data['line4'],
            'county'=>$data['county'],
            'postcode'=>$data['postcode'],
            'speaker'=>$speaker,
            'booker'=>$booker,
            'created_at'=>date("Y-m-d H:i:s"),
        ]);
        
        //dd($request->all());
      
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
