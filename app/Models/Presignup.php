<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Presignup extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'mobile',
        'roles',
        'line1',
        'line2',
        'line3',
        'line4',
        'county',
        'postcode',

    ];

    protected $casts = [
        'roles' => 'array',
        
        
    ];
}
