<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    //
	protected $table 		= 'sectors';
	protected $primaryKey	= 'sec_id';
	
    public function profile()
        {
            return $this->belongsTo(Profile::class,'pro_id');
        }

	
}
