<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'surname',
        'phone',
        'postcode',
        'contactable',
        'city',
        'contact_details'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'usr_id');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'adr_id');
    }

    public function assests()
    {
        return $this->hasMany(Assest::class, 'ass_id');
    }


    public function expertises()
    {
        return $this->belongsToMany('App\Models\Expertise','expertise_profile','pro_id','aex_id');
    }

    public function speakers()
    {
        return $this->belongsToMany('App\Models\Speaker','profile_speaker','pro_id','spk_id');
    }

    public function sector()
    {
        return $this->hasOne(Sector::class, 'sec_id');
    }
}
