<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assest extends Model
{
    use HasFactory;

    
    protected $fillable = [
        'name',
        'description',
        'filename',
        'filesize_kilobytes',
        'moderated',
    ];

    public function profile()
    {
        return $this->belongsTo(Profile::class, 'pro_id');
    }
}
