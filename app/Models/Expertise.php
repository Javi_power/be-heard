<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expertise extends Model
{
    //
	protected $table 		= 'areas_expertise';
	protected $primaryKey	= 'aex_id';
	
	public function profiles()
    {
        return $this->belongsToMany('App\Model\Profile');
    }
	
}
