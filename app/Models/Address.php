<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table 		= 'addresses';
	protected $primaryKey	= 'adr_id';
	
	const counties = array(
				'Avon'         => 'Avon',
				'Bedfordshire' => 'Bedfordshire',
				'Berkshire' => 'Berkshire',
				'Buckinghamshire' => 'Buckinghamshire',
				'Cambridgeshire' => 'Cambridgeshire',
				'Cheshire' => 'Cheshire',
				'Cleveland' => 'Cleveland',
				'Cornwall' => 'Cornwall',
				'Cumbria' => 'Cumbria',
				'Derbyshire' => 'Derbyshire',
				'Denbighshire' => 'Denbighshire',
				'Devon' => 'Devon',
				'Dorset' => 'Dorset',
				'Durham' => 'Durham',
				'East Sussex' => 'East Sussex',
				'Essex' => 'Essex',
				'Gloucestershire' => 'Gloucestershire',
				'Hampshire' => 'Hampshire',
				'Herefordshire' => 'Herefordshire',
				'Hertfordshire' => 'Hertfordshire',
				'Isle of Wight' => 'Isle of Wight',
				'Kent' =>'Kent',
				'Lancashire' => 'Lancashire',
				'Leicestershire' => 'Leicestershire',
				'Lincolnshire' => 'Lincolnshire',
				'London' => 'London',
				'Merseyside' => 'Merseyside',
				'Middlesex' => 'Middlesex',
				'Norfolk' => 'Norfolk',
				'Northamptonshire' => 'Northamptonshire',
				'Northumberland' => 'Northumberland',
				'North Humberside' => 'North Humberside',
				'North Yorkshire' => 'North Yorkshire',
				'Nottinghamshire' => 'Nottinghamshire',
				'Oxfordshire' => 'Oxfordshire',
				'Rutland' => 'Rutland',
				'Shropshire' => 'Shropshire',
				'Somerset' => 'Somerset',
				'South Humberside' => 'South Humberside',
				'South Yorkshire' =>'South Yorkshire',
				'Staffordshire' => 'Staffordshire',
				'Suffolk' => 'Suffolk',
				'Surrey' => 'Surrey',
				'Tyne and Wear' => 'Tyne and Wear',
				'Warwickshire' => 'Warwickshire',
				'West Midlands' => 'West Midlands',
				'West Sussex' => 'West Sussex',
				'West Yorkshire' => 'West Yorkshire',
				'Wiltshire' => 'Wiltshire',
				'Worcestershire' => 'Worcestershire'
			  );


    public function profile()
    {
        return $this->belongsTo(Profile::class,'pro_id');
    }
}
