<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model
{
    //
	protected $table 		= 'speakers';
	protected $primaryKey	= 'spk_id';
	
	public function profiles()
    {
        return $this->belongsToMany('App\Model\Profile');
    }
	
}